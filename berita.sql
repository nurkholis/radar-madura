-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Jan 2021 pada 02.07
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `radar_madura`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `no_npwp` varchar(40) NOT NULL,
  `id_jenis_iklan` int(11) NOT NULL,
  `judul_berita` varchar(255) NOT NULL,
  `file_berita` text DEFAULT NULL,
  `tanggal_berita` date NOT NULL,
  `bulan_muat` varchar(15) DEFAULT NULL,
  `tanggal_muat` varchar(255) DEFAULT NULL,
  `cover_berita` text DEFAULT NULL,
  `ukuran_kesamping` int(11) NOT NULL,
  `ukuran_kebawah` int(11) NOT NULL,
  `harga` varchar(11) NOT NULL,
  `dimuat` enum('0','1') NOT NULL,
  `no_invoice` varchar(30) NOT NULL,
  `dp_bayar` varchar(11) NOT NULL,
  `sisa_bayar` varchar(11) NOT NULL,
  `nama_ae` varchar(50) NOT NULL,
  `tanggal_muat_berita` varchar(255) DEFAULT NULL,
  `status_dimuat` varchar(255) DEFAULT NULL,
  `ucode_berita` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
