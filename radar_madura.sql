/*
SQLyog Professional v12.5.1 (64 bit)
MySQL - 10.4.10-MariaDB : Database - radar_madura
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`radar_madura` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `radar_madura`;

/*Table structure for table `berita` */

DROP TABLE IF EXISTS `berita`;

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `no_npwp` varchar(40) NOT NULL,
  `id_jenis_iklan` int(11) NOT NULL,
  `judul_berita` varchar(255) NOT NULL,
  `file_berita` text DEFAULT NULL,
  `tanggal_berita` date NOT NULL,
  `bulan_muat` varchar(15) DEFAULT NULL,
  `tanggal_muat` varchar(255) DEFAULT NULL,
  `cover_berita` text DEFAULT NULL,
  `ukuran_kesamping` int(11) NOT NULL,
  `ukuran_kebawah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `dimuat` enum('0','1') NOT NULL,
  `no_invoice` varchar(30) NOT NULL,
  `dp_bayar` int(11) NOT NULL,
  `sisa_bayar` int(11) NOT NULL,
  `nama_ae` varchar(50) NOT NULL,
  `tanggal_muat_berita` varchar(255) DEFAULT NULL,
  `status_dimuat` varchar(255) DEFAULT NULL,
  `ucode_berita` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `berita` */

insert  into `berita`(`id_berita`,`id_user`,`no_npwp`,`id_jenis_iklan`,`judul_berita`,`file_berita`,`tanggal_berita`,`bulan_muat`,`tanggal_muat`,`cover_berita`,`ukuran_kesamping`,`ukuran_kebawah`,`harga`,`dimuat`,`no_invoice`,`dp_bayar`,`sisa_bayar`,`nama_ae`,`tanggal_muat_berita`,`status_dimuat`,`ucode_berita`) values 
(26,8,'444444444',5,'hgjgh','','2020-07-20',NULL,'N;','',1,1,1,'0','aaaa',1,0,'TESSSSSS','01-07-2020','ya','2882849214'),
(27,8,'444444444',5,'hgjgh','','2020-07-20',NULL,'N;','',1,1,1,'0','aaaa',1,0,'TESSSSSS','08-07-2020','tidak','2882849214'),
(28,8,'444444444',5,'hgjgh','','2020-07-20',NULL,'N;','',1,1,1,'0','aaaa',1,0,'TESSSSSS','23-07-2020','ya','2882849214'),
(29,8,'444444444',5,'hgjgh','','2020-07-20',NULL,'N;','',1,1,1,'0','aaaa',1,0,'TESSSSSS','29-07-2020','tidak','2882849214'),
(30,8,'444444444',5,'hgjgh','','2020-07-21',NULL,'N;','',2,4,1,'0','aaaa',1,0,'TES2','01-07-2020','tidak','8346342553'),
(31,8,'444444444',5,'hgjgh','','2020-07-21',NULL,'N;','',2,4,1,'0','aaaa',1,0,'TES2','29-07-2020','tidak','8346342553');

/*Table structure for table `jenis_iklan` */

DROP TABLE IF EXISTS `jenis_iklan`;

CREATE TABLE `jenis_iklan` (
  `id_jenis_iklan` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nama_jenis_iklan` varchar(200) NOT NULL,
  PRIMARY KEY (`id_jenis_iklan`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `jenis_iklan` */

insert  into `jenis_iklan`(`id_jenis_iklan`,`id_user`,`nama_jenis_iklan`) values 
(5,8,'iklan teks'),
(8,8,'iklan kolom'),
(9,8,'jenis 2');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `pemasang` */

DROP TABLE IF EXISTS `pemasang`;

CREATE TABLE `pemasang` (
  `no_npwp` varchar(40) NOT NULL,
  `nama_pemasang` varchar(200) NOT NULL,
  `nama_perusahaan` varchar(200) NOT NULL,
  `alamat_pemasang` varchar(200) NOT NULL,
  `telepon_pemasang` varchar(15) NOT NULL,
  `pengenal` varchar(50) DEFAULT NULL,
  `no_id` varchar(255) NOT NULL,
  PRIMARY KEY (`no_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemasang` */

insert  into `pemasang`(`no_npwp`,`nama_pemasang`,`nama_perusahaan`,`alamat_pemasang`,`telepon_pemasang`,`pengenal`,`no_id`) values 
('','ad','faew','dadf','342','ktp','1234567890123456'),
('','ope','3123','asdasd','21323','npwp','99.999.999.9-999.999');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id_user`,`nama_user`,`email`,`password`,`api_token`,`remember_token`,`created_at`,`updated_at`) values 
(7,'Nur Kholis','dul@mail.com','$2y$10$1Q4GHEMzMj4SSWKFy334WeJxVwV0zLnXXMuZ3456C6/.YNFORA1FG','$2y$10$KgVI0XT9563c5Asc6bubCejvOPSOfvlRyqDsOaXzDBv5xEOo5/P9G','keXvqu79WrvPW6GzVyl1BcJ2zGmrE0FShlxuqll2vfTQATKCEc8EzBmvaMwv','2017-11-11 00:33:43','2017-11-11 00:33:43'),
(8,'Admin','user@mail.com','$2y$10$8OsL2f1Fo/z.t9IP.f2u6.22eg7UcjmT6mBFLNJdg3R7KliBevcUe','$2y$10$ewUrGAakTCgbcxzCS9WGq.QG.y7WZrAFA0YwcOECO9fB6bRccq6pG','KJvD4QrOm5tb4BMKM4Fw8y4uUhPQ2pVTkHGc4HQww8m5h4M2DEE0SFRBnyXg','2017-11-12 06:29:44','2017-11-12 06:29:44');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
