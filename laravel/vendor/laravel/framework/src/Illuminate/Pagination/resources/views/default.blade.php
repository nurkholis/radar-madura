@if ($paginator->hasPages())

    <nav class="navigation pagination justify-content-center" role="navigation">
        <div class="nav-links">

            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <a class="prev page-numbers disabled">
                    <i class="fa fa-chevron-left"></i>
                    <span class="screen-reader-text">Previous page</span>
                </a>
            @else
                <a class="prev page-numbers " href="{{ $paginator->previousPageUrl() }}" rel="prev">
                    <i class="fa fa-chevron-left"></i>
                    <span class="screen-reader-text">Previous page</span>
                </a>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <a class="page-numbers disabled">
                        <span class="meta-nav screen-reader-text">Page </span>
                        {{ $element }}
                    </a>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="page-numbers active">
                                <span class="meta-nav screen-reader-text">Page </span>
                                {{ $page }}
                            </a>
                        @else
                            <a class="page-numbers" href="{{ $url }}">
                                <span class="meta-nav screen-reader-text">Page </span>
                                {{ $page }}
                            </a>
                        @endif
                    @endforeach
                @endif

            @endforeach

             {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a class="prev page-numbers" href="{{ $paginator->nextPageUrl() }}" rel="prev">
                    <i class="fa fa-chevron-right"></i>
                    <span class="screen-reader-text">Next page</span>
                </a>
            @else
                <a class="prev page-numbers disabled">
                    <i class="fa fa-chevron-right"></i>
                    <span class="screen-reader-text">Next page</span>
                </a>
            @endif

        </div>
    </nav>

    {{-- <ul class="pagination">

        @if ($paginator->onFirstPage())
            <li class="disabled"><span>&laquo;</span></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        @foreach ($elements as $element)
        
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><span>{{ $page }}</span></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
            <li class="disabled"><span>&raquo;</span></li>
        @endif
    </ul> --}}
@endif
