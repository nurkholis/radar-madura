<?php

namespace Config;
use App\Http\Controllers\Controller;

class Kholis extends Controller
{ 
    public static function coba(){
        dd(0);
    }
    public static function tanggal($tanggal_berita){
        $timestamp = strtotime($tanggal_berita);
        $day = date('D', $timestamp);
        $pecah = explode("-",$tanggal_berita);
        if( count( $pecah ) < 2 ){
            return "";
        }
        switch ($day){
            case 'Mon':
                $day = 'Senin';break;
            case 'Tue':
                $day = 'Selasa';break;
            case 'Wed':
                $day = 'Rabu'; break;
            case 'Thu':
                $day = 'Kamis';break;
            case 'Fri':
                $day = 'Jumat';break;   
            case 'Sat':
                 $day = 'Sabtu';break;
            case 'Sun':
                $day = 'Minggu'; break;
            default:
        }
        $month = date('M', $timestamp);
        switch ($month){
            case 'Jan':
                $month = 'Januari';break;
            case 'Feb':
                $month = 'Faburari';break;
            case 'Mar':
                $month = 'Maret'; break;
            case 'Apr':
                $month = 'April';break;
            case 'May':
                $month = 'Mei';break;   
            case 'Jun':
                 $month = 'Juni';break;
            case 'Jul':
                $month = 'Juli'; break;
            case 'Aug':
                $month = 'Agustus';break;
            case 'Sep':
                $month = 'September';break;
            case 'Oct':
                $month = 'Oktober'; break;
            case 'Nov':
                $month = 'November';break;
            case 'Dec':
                $month = 'Desember';break;
            default:
        }
        $jadi = $day . ', ' . $pecah[2] .' ' . $month . ' ' . $pecah[0];
        return $jadi;
    }

    public static function share($media, $url, $judul = null)
    {
        $host = 'http://localhost';
        if( $media  == 'facebook' ){
            return 'https://www.facebook.com/sharer.php?u=' . $host . $url;
        }
        else if($media == 'twitter'){
            return 'https://twitter.com/intent/tweet?text=' . $judul . '. kunjungi '. $host . $url;
        }
        else if($media == 'whatsapp'){
            return 'https://api.whatsapp.com/send?text=' . $judul . '. kunjungi '. $host . $url;
        }else{
            return 'https://plus.google.com/share?url=' . $host . $url;
        }
    }
}