<!DOCTYPE html>
<html class="no-js">
<head>
	<title>PMII</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="{{ asset('public/assets/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/assets/css/animations.css') }}" rel="stylesheet">
	<link href="{{ asset('public/assets/css/font-awesome.css') }}" rel="stylesheet">
	<link href="{{ asset('public/assets/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('public/assets/css/visitor.css') }}" rel="stylesheet">
	<style>
		.preloader_image {
			background: url("{{ asset('public/images/web/logo.png') }}") no-repeat 50% 50% transparent;
		}
		.page_404 {
			background-image: url("{{ asset('public/images/web/bg/404.png') }}");
			background-position: left;
		}
	</style>
	<script src="{{ asset('public/assets/js/vendor/modernizr-custom.js') }}"></script>
	@yield('css')

</head>

<body>
	<div class="preloader">
		<div class="preloader_image pulse"></div>
	</div>

	<!-- search modal -->
	<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="widget widget_search">
			<form method="get" class="searchform search-form" action="{{ route('visitor.berita.index') }}">
				<div class="form-group">
					<input type="text" value="" name="search" class="form-control" placeholder="cari di website" id="modal-search-input">
				</div>
				<button type="submit"></button>
			</form>
		</div>
	</div>

	<!-- Unyson messages modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
		<div class="fw-messages-wrap ls p-normal">
			<!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
			<!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->

		</div>
	</div><!-- eof .modal -->

	<!-- wrappers for visual page editor and boxed version of template -->
	<div id="canvas">
		<div id="box_wrapper">
			<!-- header -->
			<section class="page_toplogo ls s-pt-45 s-pb-40 d-none d-lg-block">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-lg-3">
							<div class="">
								<a href="index-2.html" class="">
									<img src="{{ asset('public/images/web/logo.png') }}" alt="img">
								</a>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="d-flex justify-content-center">
								<div class="media header-media">
									<div class="media-body">
										<h4>
											E-MAIL
										</h4>
										<p><a href="mailto:pmii.pamekasan@gmail.com">pmii.pamekasan@gmail.com</a></p>
									</div>
								</div>

								<div class="media header-media">
									<div class="media-body">
										<h4>
											KANTOR
										</h4>
										<p>Jl. Brawijaya No 52B Pamekasan</p>
									</div>
								</div>

								<div class="media header-media">
									<div class="media-body">
										<h4>
											TELEPON
										</h4>
										<p>
											<a href="tel:082335996777">082335996777</a>
										</p>
									</div>
								</div>


							</div>
						</div>

					</div>
				</div>
			</section>

			<!-- navbar -->
			<header class="page_header ls bg-maincolor4 main-style">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-xl-12">
							<div class="nav-wrap header-main">
								<a href="index-2.html" class="logo logo-ds">
									<img src="{{ asset('public/images/web/pmii_small.png') }}" alt="img">
								</a>
								<!-- main nav start -->
								<nav class="top-nav">
									<ul class="nav sf-menu">
										<li class=""><a href="{{ route('visitor.home.index') }}">Beranda</a></li>
										<li class=""><a href="{{ route('visitor.sejarah.index') }}">Sejarah</a></li>
										<li class=""><a href="{{ route('visitor.berita.index') }}">berita</a></li>
										<li class=""><a href="{{ route('visitor.artikel.index') }}">artikel</a></li>
										<li class=""><a href="{{ route('visitor.materi.index') }}">materi</a></li>
										
									</ul>


								</nav>
								<!-- eof main nav -->
								<span class="mr-5">
									<a href="#" class="search_modal_button hidden-below-xl">
										<i class="fa fa-search"></i>
									</a>
								</span>
								<ul class="top-includes d-none d-xl-block">
									<li>
										<span class="social-icons">

											<a href="https://twitter.com/PamekasanPc" target="_blank" class="fa fa-twitter border-icon rounded-icon color-icon" title="twitter"></a>
											<a href="https://www.youtube.com/channel/UCNoKrraABABeM4ksFEYqWIw" target="_blank" class="fa fa-youtube border-icon rounded-icon color-icon" title="youtube"></a>
											<a href="https://www.facebook.com/PC-PMII-pamekasan-703738236415500/" target="_blank" class="fa fa-facebook border-icon rounded-icon color-icon" title="facebook"></a>
											<a href="https://www.instagram.com/pmiipamekasan/" target="_blank" class="fa fa-instagram border-icon rounded-icon color-icon" title="instagram"></a>

										</span>
									</li>
								</ul>
							</div>

						</div>
					</div>
				</div>
				<!-- header toggler -->
				<span class="toggle_menu"><span></span></span>
			</header>
			
			@yield('content')

			<footer class="page_footer ds s-pb-35 s-pt-60 s-pb-md-70 s-pt-md-90 s-pb-xl-130 s-pt-xl-160 c-mb-20 c-gutter-30 container-px-0">

				<div class="container">
					<div class="row">

						<div class="col-lg-6 text-center text-lg-left animate" data-animation="fadeInUp">

							<div class="widget widget_icons_list">
								<h3 class="widget-title">Hubungi Kami</h3>

								<ul class="mb-10">
									<li class="icon-inline">
										<div class="icon-styled icon-top color-main4 fs-14">
											<i class="fa fa-map-marker"></i>
										</div>
										<p>Jl. Brawijaya No 52B Pamekasan</p>
									</li>
									<li class="icon-inline">
										<div class="icon-styled icon-top color-main4 fs-14">
											<i class="fa fa-envelope"></i>
										</div>
										<p><a href="mailto:pmii.pamekasan@gmail.com">pmii.pamekasan@gmail.com</a></p>
									</li>
									<li class="icon-inline">
										<div class="icon-styled icon-top color-main4 fs-14">
											<i class="fa fa-phone"></i>
										</div>
										<a href="tel:082335996777">082335996777</a>
									</li>
								</ul>

								<span class="social-icons">
										<a href="https://twitter.com/PamekasanPc" target="_blank" class="fa fa-twitter border-icon rounded-icon footer-icon" title="twitter"></a>
										<a href="https://www.youtube.com/channel/UCNoKrraABABeM4ksFEYqWIw" target="_blank" class="fa fa-youtube border-icon rounded-icon footer-icon" title="youtube"></a>
										<a href="https://www.facebook.com/PC-PMII-pamekasan-703738236415500/" target="_blank" class="fa fa-facebook border-icon rounded-icon footer-icon" title="facebook"></a>
										<a href="https://www.instagram.com/pmiipamekasan/" target="_blank" class="fa fa-instagram border-icon rounded-icon footer-icon" title="instagram"></a>
								</span>
							</div>
						</div>

						<div class="col-lg-6 text-center text-lg-left animate" data-animation="fadeInUp">
							<div class="widget mb-0">

								<h3 class="widget-title">PMII Pamekasan App</h3>

								<img src="{{ asset('public/images/web/googleplay.png') }}" style="width:300px" alt="">

								<button href="#" class="btn btn-maincolor mt-30">
									Download
								</button>

							</div>
						</div>
					</div>
				</div>
			</footer>

			<section class="page_copyright ds s-py-5">
				<div class="container border-top-color">
					<div class="row align-items-center ">
						<div class="divider-20 d-none d-lg-block"></div>
						<div class="divider-10 d-none d-md-block d-lg-none"></div>
						<div class="col-md-12 text-center">
							<p>&copy; Copyright <span class="copyright_year">2019</span></p>
						</div>
						<div class="divider-20 d-none d-lg-block"></div>
						<div class="divider-10 d-none d-md-block d-lg-none"></div>
					</div>
				</div>
			</section>


		</div><!-- eof #box_wrapper -->
	</div><!-- eof #canvas -->

	<script src="{{ asset('public/assets/js/compressed.js') }}"></script>
	<script src="{{ asset('public/assets/js/main.js') }}"></script>
	@yield('js')

</body>

</html>