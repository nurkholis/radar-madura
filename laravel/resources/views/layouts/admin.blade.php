<!DOCTYPE html>
<html lang="en">

	
<!-- Mirrored from theembazaar.com/demo/themejio/truckry/v1.1/Vertical/grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Jan 2019 01:19:45 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>RADAR MADURA</title>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<!-- google font -->
		<link href="{{ asset('public/admin/assets/fonts.googleapis.com/css6079.css?family=Poppins:300,400,500,600,700') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('public/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('public/admin/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('public/admin/assets/css/ionicons.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('public/admin/assets/css/simple-line-icons.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('public/admin/assets/css/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
		<link href="{{ asset('public/admin/assets/css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('public/admin/assets/css/header.css') }}" rel="stylesheet">
		<link href="{{ asset('public/admin/assets/css/menu.css') }}" rel="stylesheet">
		<link href="{{ asset('public/admin/assets/css/responsive.css') }}" rel="stylesheet">
		<link href="{{ asset('public/admin/assets/css/custom.css') }}" rel="stylesheet">
		@yield('css')
	</head>

	<body>
		<div class="wrapper">
			<!-- header -->
			<div class="header-bg">
				<header class="main-header">
					<div class="container_header phone_view border_top_bott">

							<div class="row">
								<div class="col-md-12">
									<div class="logo d-flex align-items-center">
										<a href="{{ route('admin.home') }}"> 
											<strong class="logo_icon"> <img src="{{ asset('public/images/logo.png') }}" alt=""> </strong> 
											<span class="logo-default"> <img src="{{ asset('public/images/logo.png') }}" alt="" class="d-none d-lg-block">
											<img src="{{ asset('public/images/logo.png') }}" alt="" class="d-block d-lg-none"> </span> 
										</a>
										<div class="icon_menu">
											<a href="#" class="menu-toggler sidebar-toggler"></a>
										</div>
									</div>

									<div class="right_detail">
										<div class="row d-flex align-items-center justify-content-end">
										<div class="col-xl-7 col-12 d-flex justify-content-end">
												<div class="right_bar_top d-flex align-items-center">
													<div class="search">
														<div class="d-lg-none">
															<a id="toggle_res_search" data-toggle="collapse" data-target="#search_form" class="res-only-view collapsed" href="javascript:void(0);" aria-expanded="false"> <i class=" icon-magnifier"></i> </a>
															<form id="search_form" role="search" class="search-form collapse" action="#">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="Search...">
																	<button type="button" class="btn" data-target="#search_form" data-toggle="collapse" aria-label="Close">
																		<i class="ion-android-search"></i>
																	</button>
																</div>
															</form>
														</div>
													</div>

													<!-- reminder -->
													@include('layouts.notif')
													<!-- Dropdown_User -->
													<div class="dropdown dropdown-user">
														<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <span class="name_admin">{{ Auth::guard('web')->user()->nama_user }} <i class="fa fa-angle-down" aria-hidden="true"></i></span> </a>
														<ul class="dropdown-menu dropdown-menu-default">
															<li>
																<a href="{{ route('admin.profil.index') }}"> <i class="icon-user"></i> Profile </a>
															</li>
															<li>
																<a href="{{ route('admin.logout') }}"> <i class="icon-logout"></i> Log Out </a>
															</li>
														</ul>
													</div>
													

												</div>
											</div>

										</div>
									</div>

								</div>
							</div>
						</div>
					
				</header>

				

			</div>
			<!-- header_End -->
			<!-- Content_right -->
			<div class="container_full">
				<div class="side_bar scroll_auto">
					<ul id="dc_accordion" class="sidebar-menu tree">
						<li class="menu_sub">
							<a href="{{ route('admin.home') }}"> <i class="fa fa-home"></i> <span>Beranda</span></a>
							@if (Auth::guard('web')->user()->level == '0')
								<a href="{{ route('admin.admin.index') }}"> <i class="fa fa-shield"></i> <span>Admin</span></a>
							@endif
							
							<a href="{{ route('admin.berita.index') }}"> <i class="fa fa-newspaper-o"></i> <span>Data Iklan</span></a>
							<a href="{{ route('admin.pemasang.index') }}"> <i class="fa fa-user-o"></i> <span>Pemasang</span></a>
							<a href="{{ route('admin.jenisIklan.index') }}"> <i class="fa fa-star-o"></i> <span>Jenis Iklan</span></a>
						</li>
						
					</ul>

				</div>
				<!--main contents start-->
				<main class="content_wrapper">
					@yield('content')
				</main>
				<!--main contents end-->

			</div>
			<!-- Content_right_End -->
			<!-- Footer -->
			<footer class="footer ptb-20">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="copy_right">
							<p>
								 © Radar Madura
							</p>
						</div>
						<a id="back-to-top" href="#" style="display: inline;"> <i class="ion-android-arrow-up"></i> </a>
					</div>
				</div>
			</footer>
			<!-- Footer_End -->
		</div>
		<script type="text/javascript" src="{{ asset('public/admin/assets/js/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('public/admin/assets/js/popper.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('public/admin/assets/js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('public/admin/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('public/admin/assets/js/jquery.dcjqaccordion.2.7.js') }}"></script>
		<script type="text/javascript" src="{{ asset('public/admin/assets/js/custom.js') }}"></script>
		<script>
			function confirmDelete(){
				if (!confirm("hapus data?")) {
					event.preventDefault()
				}
			}
			function confirmDimuat(){
				if (!confirm("berita sudah dimuat?")) {
					event.preventDefault()
				}
			}
			function confirmReset(){
				if (!confirm("reset?")) {
					event.preventDefault()
				}
			}
		</script>
		@yield('js')
	</body>

</html>