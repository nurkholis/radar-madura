@php
    use App\Berita;
    use Carbon\Carbon;
    $segera_dimuat = Berita::where('status_dimuat', 'tidak')
        ->leftJoin('pemasang', 'pemasang.no_id', '=', 'berita.no_npwp')
        ->where("tanggal_muat_berita", '=',  Carbon::tomorrow()->format('d-m-Y'))
        ->orWhere("tanggal_muat_berita", '=',  Carbon::now()->format('d-m-Y'))
        ->orderBy("tanggal_muat_berita", "ASC")
        ->get();
@endphp
<div class="dropdown dropdown-notification">
    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
        <i class="fa fa-bell-o"></i>
        @if ($segera_dimuat->count() > 0 )
            <span class="badge_coun"> {{ $segera_dimuat->count() }} </span> 
        @endif
    </a>
    <ul class="dropdown-menu scroll_auto height_fixed mCustomScrollbar _mCS_1" x-placement="top-start" style="position: absolute; transform: translate3d(-95px, 5px, 0px); top: 0px; left: 0px; will-change: transform;"><div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container" style="position: relative; top: 0px; left: 0px;" dir="ltr">
        <li class="bigger">
            <h3><span class="bold">Notifikasi</span></h3>
        </li>
        <li>
            <ul class="dropdown-menu-list">
                @foreach ($segera_dimuat as $item)
                    <li>
                        <a href="{{ route('admin.berita.index') }}">
                            <span class="time">{{ $item->tanggal_muat_berita ==  Carbon::now()->format('d-m-Y') ? "Hari ini" : "Besok" }}</span> <span class="details"> 
                            <span class="notification-icon yellow"> <i class="fa fa-info"></i> 
                            </span> INV {{ $item->no_invoice }}</span> 
                            <span class="message"> Judul  {{ $item->judul_berita }}, Pemasang {{ $item->nama_pemasang }}</span> 
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
    </div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: block;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; display: block; height: 242px; max-height: 290px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></ul>
</div>