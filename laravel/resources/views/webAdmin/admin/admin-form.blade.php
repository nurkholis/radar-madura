@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Admin</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                            <form method="POST" action="{{ route('admin.admin.put', $edit->id_user) }}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                        @else
                            <form method="POST" action="{{ route('admin.admin.store') }}" enctype="multipart/form-data">
                        @endif
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">nama admin</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="nama_user"
                                    @if( isset($edit) ) value="{{ $edit->nama_user }}" @else value="{{ old('nama_user') }}" @endif required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">email</label>
                                <div class="col-sm-7">
                                    <input type="email" class="form-control" name="email"
                                    @if( isset($edit) ) value="{{ $edit->email }}" @else value="{{ old('email') }}" @endif required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">password</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="password" required>
                                </div>
                            </div>

                                <div class="float-right">
                                    <a href="{{ route('admin.admin.index') }}" class="btn btn-danger ">Batal</a>
                                    <button type="submit" class="btn btn-primary ">Simpan</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('js')
<script></script>
        
@endsection