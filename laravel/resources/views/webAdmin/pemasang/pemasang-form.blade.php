@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 mx-auto">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} pemasang</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                        <form method="POST" action="{{ route('admin.pemasang.put', $edit->no_id) }}"
                            enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            @else
                            <form method="POST" action="{{ route('admin.pemasang.store') }}"
                                enctype="multipart/form-data">
                                @endif
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nomor yang digunakan</label>
                                    <div class="col-sm-7">
                                        {{-- <input type="radio" id="npwp" name="pengenal" value="npwp" checked="checked">
                                        <label for="npwp">NPWP</label><br>
                                        <input type="radio" id="ktp" name="pengenal" value="ktp">
                                        <label for="ktp">KTP</label><br> --}}
                                        <select name="pengenal" class="form-control">
                                            <option value="npwp">NPWP</option>
                                            <option value="ktp">KTP</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">no NPWP/KTP</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="ktp" name="npwp" @if( isset($edit) )
                                            value="{{ $edit->no_id }}" @else value="{{ old('npwp') }}" @endif>
                                        <input type="text" class="form-control" id="npwp" name="ktp" @if( isset($edit) )
                                            value="{{ $edit->no_id }}" @else value="{{ old('ktp') }}" @endif>
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">no NPWP/KTP</label>
                                    <div class="col-sm-7">
                                        <input type="number" class="form-control" name="no_npwp"
                                            ria-describedby="passwordHelpBlock" pattern="{20}" @if( isset($edit) )
                                            value="{{ $edit->no_npwp }}" @else value="{{ old('no_npwp') }}" @endif
                                            required>

                                        <small id="passwordHelpBlock" class="form-text text-muted">
                                            NPWP/KTP hanya masukkan angkanya saja.
                                        </small>

                                    </div>
                                </div> --}}

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">nama pemasang</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="nama_pemasang"
                                            pattern="[A-Za-z ']{1,}" title="hanya boleh huruf saja" @if( isset($edit) )
                                            value="{{ $edit->nama_pemasang }}" @else value="{{ old('nama_pemasang') }}"
                                            @endif required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">nama perusahaan</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="nama_perusahaan" @if( isset($edit)
                                            ) value="{{ $edit->nama_perusahaan }}" @else
                                            value="{{ old('nama_perusahaan') }}" @endif required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">alamat</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="alamat_pemasang" @if( isset($edit)
                                            ) value="{{ $edit->alamat_pemasang }}" @else
                                            value="{{ old('alamat_pemasang') }}" @endif required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">telepon</label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control" name="telepon_pemasang" @if(
                                            isset($edit) ) value="{{ $edit->telepon_pemasang }}" @else
                                            value="{{ old('telepon_pemasang') }}" @endif required>
                                    </div>
                                </div>

                                <div class="float-right">
                                    <a href="{{ route('admin.pemasang.index') }}" class="btn btn-danger ">Batal</a>
                                    <button type="submit" class="btn btn-primary ">Simpan</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js" integrity="sha512-d4KkQohk+HswGs6A1d6Gak6Bb9rMWtxjOa0IiY49Q3TeFd5xAzjWXDCBW9RS7m86FQ4RzM2BdHmdJnnKRYknxw==" crossorigin="anonymous"></script> --}}
    
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/jquery.mask.min.js') }}"></script>
    <script>
        jQuery(function($){
            @if(isset($edit))
                @if($edit->pengenal == "ktp")
                    $("input[name='npwp']").hide();
                @else
                    $("input[name='ktp']").hide();
                @endif
            @else
                $("input[name='ktp']").hide();
            @endif
            
            $("select[name='pengenal']").change(function (e) { 
                if ($(this).val() == "ktp") {
                    $("input[name='ktp']").show();
                    $("input[name='npwp']").hide();
                    $("input[name='npwp']").prop('required',false);
                    $("input[name='ktp']").prop('required',true);
                    jqattr
                } else {
                    $("input[name='npwp']").show();
                    $("input[name='ktp']").hide();
                    $("input[name='ktp']").prop('required',false);
                    $("input[name='npwp']").prop('required',true);
                }
            });
            $("input[name='npwp']").mask("99.999.999.9-999.999");
            $("input[name='ktp']").mask("9999999999999999");
        });
    </script>
<script>

</script>

@endsection