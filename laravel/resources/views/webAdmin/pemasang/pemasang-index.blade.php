@extends('layouts.admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/css/dataTables.bootstrap4.min.css') }}">
@endsection
@section('content')

    @php
    use Config\Kholis as Helper;
    @endphp

    <div class="container">
        <section class="chart_section">
            <div class="row">
                <div class="col-md-12 mb-4 align-items-stretch">
                    <div class="widthfull card card-shadow">
                        <div class="card-header">
                            <div class="card-title">
                               <span> Data pemasang</span>
                               <a href="{{ route('admin.pemasang.create') }}" class="btn btn-primary float-right">Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="bd-example table_style">

                                <table id="table" class="table">
                                    <thead>
                                        <tr>
                                            <th>No NPWP/KTP</th>
                                            <th>Pemasang</th>
                                            <th>Perusahaan</th>
                                            <th>Telepon</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

@endsection

@section('js')
<script type="text/javascript" language="javascript" src="{{ asset('public/admin/assets/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('public/admin/assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $( document ).ready(function() {
            $('#table').DataTable().clear();
            $('#table').DataTable().destroy(false);
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.pemasang.dataTable') }}",
                columns: [
                    {data: 'no_id'},
                    {data: 'nama_pemasang'},
                    {data: 'nama_perusahaan'},
                    {data: 'telepon_pemasang'},
                    {data: 'action'},
                ]
            });
        });
        
    </script>
@endsection