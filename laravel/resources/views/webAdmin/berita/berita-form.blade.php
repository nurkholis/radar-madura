@php
    use Carbon\Carbon;
@endphp

@extends('layouts.admin')
@section('css')
<link href="{{ asset('public/admin/assets/css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/admin/assets/datepicker/datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('public/admin/assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">{{ isset($edit) ? 'Sunting' : 'Tambah' }} Order Iklan </h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger alert-with-icon" data-notify="container">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <span data-notify="message">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                        @endif
                        @if(isset($edit))
                        <form method="POST" action="{{ route('admin.berita.put', $edit->ucode_berita) }}"
                            enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            @else
                            <form method="POST" action="{{ route('admin.berita.store') }}"
                                enctype="multipart/form-data">
                                @endif
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group row">
                                            <label class="col-sm-3">no npwp / ktp</label>
                                            <div class="col-sm-7">
                                                <select class="form-control" name="no_npwp" id="npwp"
                                                    data-plugin="select2">
                                                    @if (isset($edit))
                                                    @foreach ($pemasang as $item)
                                                    <option value="{{ $item->no_id }}"
                                                        {{ $item->no_id == $edit->no_id ? 'selected' : '' }}>
                                                        {{ $item->no_id }} {{ $item->nama_pemasang }}</option>
                                                    @endforeach
                                                    @else
                                                    @foreach ($pemasang as $item)
                                                    <option value="{{ $item->no_id }}"> {{ $item->no_id }}
                                                        {{ $item->nama_pemasang }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-sm-1">
                                                <a class="btn btn-primary"
                                                    href="{{ route('admin.pemasang.create') }}"><i
                                                        class="fa fa-plus"></i></a>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">no invoice</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="no_invoice" @if(
                                                    isset($edit) ) value="{{ $edit->no_invoice }}" @else
                                                    value="{{ old('no_invoice') }}" @endif required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama AE</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="nama_ae" @if( isset($edit)
                                                    ) value="{{ $edit->nama_ae }}" @else value="{{ old('nama_ae') }}"
                                                    @endif required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">judul iklan</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="judul_berita" 
                                                @if(isset($edit) ) 
                                                    value="{{ $edit->judul_berita }}" 
                                                @else
                                                    value="{{ old('judul_berita') }}" 
                                                @endif 
                                                required>
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">tanggal order iklan</label>
                                            <div class="col-sm-5">
                                                <input type="date" class="form-control" name="tanggal_berita" 
                                                @if(isset($edit) ) 
                                                    value="{{ $edit->tanggal_berita }}" 
                                                @else
                                                    @if (old('tanggal_berita') != null)
                                                        value="{{ old('tanggal_berita') }}" 
                                                    @else
                                                        value="{{ Carbon::now()->format('Y-m-d') }}" 
                                                    @endif
                                                @endif
                                                required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">jenis iklan</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="id_jenis_iklan">
                                                    @if (isset($edit))
                                                    @foreach ($jenis_iklan as $item)
                                                    <option value="{{ $item->id_jenis_iklan }}"
                                                        {{ $item->id_jenis_iklan == $edit->id_jenis_iklan ? 'selected' : '' }}>
                                                        {{ $item->nama_jenis_iklan }}</option>
                                                    @endforeach
                                                    @else
                                                    @foreach ($jenis_iklan as $item)
                                                    <option value="{{ $item->id_jenis_iklan }}">
                                                        {{ $item->nama_jenis_iklan }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">file iklan</label>
                                            <div class="col-sm-4">
                                                <label class="custom-file">
                                                    <input type="file" class="custom-file-input" id="input-file"
                                                        name="file_berita" accept=".pdf,.doc,.docx">
                                                    <span id="img-name" class="custom-file-control"></span></label>
                                            </div>
                                            <div class="col-sm-3 text-center">
                                                <h3 id="nama_file"></h3>
                                                @if(isset($edit))
                                                <span>baru</span>
                                                @endif
                                            </div>
                                            <div class="col-sm-3 text-center">
                                                @if(isset($edit))
                                                <h3>{{ $edit->file_berita }}</h3>
                                                <span>lama</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">foto iklan</label>
                                            <div class="col-sm-4">
                                                <label class="custom-file">
                                                    <input type="file" class="custom-file-input" id="input-cover_berita"
                                                        name="cover_berita" accept=".jpg,.png">
                                                    <span id="img-name" class="custom-file-control"></span></label>
                                            </div>
                                            <div class="col-sm-3 text-center">
                                                <img src="" id="prev-cover_berita" class="img-preview">
                                                @if(isset($edit))
                                                <span>baru</span>
                                                @endif
                                            </div>
                                            <div class="col-sm-3 text-center">
                                                @if(isset($edit))
                                                <img src="{{ asset('/public/images/berita').'/'.$edit->cover_berita }}"
                                                    alt="" class="img-preview">
                                                <span>lama</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">ukuran kesamping (mm)</label>
                                            <div class="col-sm-3">
                                                <input type="number" class="form-control" name="ukuran_kesamping" @if(
                                                    isset($edit) ) value="{{ $edit->ukuran_kesamping }}" @else
                                                    value="{{ old('ukuran_kesamping') }}" @endif required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">ukuran kebawah (mm)</label>
                                            <div class="col-sm-3">
                                                <input type="number" class="form-control" name="ukuran_kebawah" @if(
                                                    isset($edit) ) value="{{ $edit->ukuran_kebawah }}" @else
                                                    value="{{ old('ukuran_kebawah') }}" @endif required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">tanggal iklan dimuat</label>
                                            <div class="col-sm-5">
                                                <input autocomplete="off" type="text" class="form-control datepicker"
                                                    name="tanggal_muat_berita" @if( isset($edit) )
                                                    value="{{ $edit->tgl_muat }}" @else
                                                    value="{{ old('tanggal_muat_berita') }}" @endif required>
                                            </div>
                                        </div>

                                        {{-- <div class="form-group row">
                <label class="col-sm-3 col-form-label">bulan iklan dimuat</label>
                <div class="col-sm-8">
                    <select class="form-control" name="bulan_muat" id="bulan" required>
                        <option value="JANUARI" @if (isset($edit)) @if($edit->bulan_muat == 'JANUARI') selected  @endif @endif>JANUARI</option>
                        <option value="FEBRUARI" @if (isset($edit)) @if($edit->bulan_muat == 'FEBRUARI') selected  @endif @endif>FEBRUARI</option>
                        <option value="MARET" @if (isset($edit)) @if($edit->bulan_muat == 'MARET') selected  @endif @endif>MARET</option>
                        <option value="APRIL" @if (isset($edit)) @if($edit->bulan_muat == 'APRIL') selected  @endif @endif>APRIL</option>
                        <option value="MEI" @if (isset($edit)) @if($edit->bulan_muat == 'MEI') selected  @endif @endif>MEI</option>
                        <option value="JUNI" @if (isset($edit)) @if($edit->bulan_muat == 'JUNI') selected  @endif @endif>JUNI</option>
                        <option value="JULI" @if (isset($edit)) @if($edit->bulan_muat == 'JULI') selected  @endif @endif>JULI</option>
                        <option value="AGUSTUS" @if (isset($edit)) @if($edit->bulan_muat == 'AGUSTUS') selected  @endif @endif>AGUSTUS</option>
                        <option value="SEPTEMBER" @if (isset($edit)) @if($edit->bulan_muat == 'SEPTEMBER') selected  @endif @endif>SEPTEMBER</option>
                        <option value="OKTOBER" @if (isset($edit)) @if($edit->bulan_muat == 'OKTOBER') selected  @endif @endif>OKTOBER</option>
                        <option value="NOVEMBER" @if (isset($edit)) @if($edit->bulan_muat == 'NOVEMBER') selected  @endif @endif>NOVEMBER</option>
                        <option value="DESEMBER" @if (isset($edit)) @if($edit->bulan_muat == 'DESEMBER') selected  @endif @endif>DESEMBER</option>
                    </select>
                </div>
            </div>
    
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">tanggal iklan dimuat</label>
                <div class="col-sm-9">
                    <select class="form-control" id="tanggal" name="tanggal_muat[]" multiple="multiple" required>
                        <option value="1">1</option><option value="2">2</option><option value="3">3</option>
                        <option value="4">4</option><option value="5">5</option><option value="6">6</option>
                        <option value="7">7</option><option value="8">8</option><option value="9">9</option>
                        <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                        <option value="13">13</option><option value="14">14</option><option value="15">15</option>
                        <option value="16">16</option><option value="17">17</option><option value="18">18</option>
                        <option value="19">19</option><option value="20">20</option><option value="21">21</option>
                        <option value="22">22</option><option value="23">23</option><option value="24">24</option>
                        <option value="25">25</option><option value="26">26</option><option value="27">27</option>
                        <option value="28">28</option><option value="29">29</option><option value="30">30</option>
                        <option value="31">31</option><option value="32">32</option>
                    </select>
                </div>
            </div> --}}

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">harga+ppn 10% (Rp)</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="input-harga" class="form-control" name="harga"
                                                    @if( isset($edit) ) value="{{ $edit->harga }}" @else
                                                    value="{{ old('harga') }}" @endif required>
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">dp/bayar (Rp)</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="input-bayar" class="form-control"
                                                    name="dp_bayar" @if( isset($edit) )
                                                    value="{{ $edit->dp_bayar }}" @else value="{{ old('dp_bayar') }}"
                                                    @endif required>
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">sisa pembayaran (Rp)</label>
                                            <div class="col-sm-5">
                                                <input type="text" id="input-kembali" class="form-control" readonly
                                                    name="sisa_bayar" @if( isset($edit) )
                                                    value="{{ $edit->sisa_bayar }}" @else
                                                    value="{{ old('sisa_bayar') }}" @endif required>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                                <br />
                                <div class="float-right">
                                    <a href="{{ route('admin.berita.index') }}" class="btn btn-danger ">Batal</a>
                                    <button type="submit" id="btn-simpan" class="btn btn-primary hide">Simpan</button>
                                    <button type="button" id="btn-bayar" class="btn btn-primary ">Bayar</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    class="modal fade text-left">
    <div role="document" class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Cari pemasang</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-11">
                        <div class="input-group">
                            <input type="text" id="input-cari-pemasang" class="form-control">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="col-md-1">
                        <a href="{{ route('admin.pemasang.index') }}" target="_blank" class="btn btn-primary"><i
                                class="fa fa-plus"></i></a>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive table-sell-item">
                            <table class="table table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th>no NPWP</th>
                                        <th>pemasang</th>
                                        <th>perusahaan</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="isi-tabel-pemasang">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="{{ asset('public/admin/assets/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/assets/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/assets/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/assets/js/jquery.mask.min.js') }}"></script>
<script>

    const format = num => {
        const n = String(num),
            p = n.indexOf('.')
        return n.replace(
            /\d(?=(?:\d{3})+(?:\.|$))/g,
            (m, i) => p < 0 || i < p ? `${m}.` : m
        )
    }

    $("input[name='harga']").mask("#.##0", {reverse: true});
    $("input[name='dp_bayar']").mask("#.##0", {reverse: true});
    $("input[name='sisa_bayar']").mask("#.##0", {reverse: true});
    // datepicker multidate
    $('.datepicker').datepicker({
        multidate: true,
        format: 'dd-mm-yyyy',
        clearBtn: true
    });
    $('#npwp').select2();
    $('#bulan').select2();
    $('#tanggal').select2();
    $("#input-file").change(function () {
        var input = this;
        if (input.files && input.files[0]) {
            $('#nama_file').html(input.files[0]['name']);
        }
    });

    $("#input-cover_berita").change(function () {
        var input = this;
        if (input.files && input.files[0]) {
            console.log(input.files[0].name);
            $("#img-name").html(input.files[0].name)
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#prev-cover_berita').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    });

    $("#btn-simpan").hide();
    @if(isset($edit))

    @if(unserialize($edit -> tanggal_muat))
    var selectedValues = new Array();
    @foreach(unserialize($edit -> tanggal_muat) as $item)
    selectedValues[{
        {
            $loop -> iteration - 1
        }
    }] = '{{ $item }}';
    @endforeach
    $('#tanggal').val(selectedValues).trigger('change');
    @endif
    @endif

    $("#btn-bayar").on("click", function () {
        var harga = $("#input-harga").val();
        var bayar = $("#input-bayar").val();
        harga = parseInt(harga.replaceAll(".", ""));
        bayar = parseInt(bayar.replaceAll(".", ""));
        if (bayar > harga) {
            alert('Uangnya terlalu banyak!');
        } else {
            var kembali = harga - bayar;
            kembali = format(kembali);
            $("#input-kembali").val(kembali);
            $("#btn-bayar").hide();
            $("#btn-simpan").show();
        }
    });
</script>
@endsection