@php
	use Config\Kholis as Helper;
@endphp
@extends('layouts.admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/css/dataTables.bootstrap4.min.css') }}">
@endsection
@section('content')

    <div class="container">
        <div class="row">

			{{-- detail berita--}}
        	<div class="col-md-6">
				<div class="card card-shadow widthfull">
					<div class="card-header">
						<div class="card-title">
							Detail Barita
						</div>
					</div>
					<div class="card-body">
						<table class="table table-light">
							<tbody>
									<tr>
										<td><strong class="text-capitalize">judul berita</strong></td>
										<td>{{ $berita->judul_berita }}</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">tanggal berita</strong></td>
										<td>{{ Helper::tanggal($berita->tanggal_berita) }}</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">cover berita</strong></td>
										<td>
											<a href="{{ asset('public/images/berita') }}/{{ $berita->cover_berita }}">
												<img src="{{ asset('public/images/berita') }}/{{ $berita->cover_berita }}" style="" alt="">
											</a>
										</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">nama pemasang</strong></td>
										<td>{{ $berita->nama_pemasang }}</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">alamat pemasang</strong></td>
										<td>{{ $berita->alamat_pemasang }}</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">telepon pemasang</strong></td>
										<td>{{ $berita->telepon_pemasang }}</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">jenis iklan</strong></td>
										<td>{{ $berita->nama_jenis_iklan }}</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">tanggal berita</strong></td>
										<td>{{ $berita->tanggal_berita }}</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">ukuran kesamping </strong></td>
										<td>{{ $berita->ukuran_kesamping }} mm</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">ukuran kebawah </strong></td>
										<td>{{ $berita->ukuran_kebawah }} mm</td>
									</tr>
									<tr>
										<td><strong class="text-capitalize">harga</strong></td>
										<td>Rp {{ $berita->harga }}</td>
									</tr>
									{{-- <tr>
										<td><strong class="text-capitalize">dimuat</strong></td>
										<td>
											@if ($berita->dimuat)
												<span class="badge badge-pill badge-primary">Ya</span>
											@else
												<span class="badge badge-pill badge-danger">Tidak</span>
											@endif
										</td>
									</tr> --}}
							</tbody>
						</table>
					</div>
				</div>
			</div>

			{{-- detail pemuatan --}}
			<div class="col-md-6">
					<div class="card card-shadow" style="width: 1000%; height: inherit">
						<div class="card-header">
							<div class="card-title">
								Detail Pemuatan
							</div>
						</div>
						<div class="card-body">
							<div class="baseline baseline-border">
								@if ($berita->tgl_muat != "")
									@foreach(explode(',', $berita->tgl_muat) as $tgl) 
										@php
											$tgls = explode(';', $tgl);
											$tag = $tgls[1]=="ya"?"baseline-success":"baseline-warning";
											$cap = $tgls[1]=="ya"?"Sudah dimuat":"Belum dimuat";
										@endphp
										<div class="baseline-list baseline-border {{$tag}}">
											<div class="baseline-info">
											<span class="text-muted">{{ $tgls[0] }} ({{$cap}})</span>
											</div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
			
        </div>
    </div>

@endsection

@section('js')
    
@endsection