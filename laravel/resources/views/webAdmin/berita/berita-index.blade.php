@extends('layouts.admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/css/dataTables.bootstrap4.min.css') }}">
<link href="{{ asset('public/admin/assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <style>
        ul.tgl_muat {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        ul.tgl_muat li {
            font-size: 13px;
            height: 21px;
        }
        ul.stat_muat {
            margin: 0;
            padding: 0;
            list-style: none;
            margin-top: 5px;
        }

        ul.stat_muat li {
            margin-bottom: 0px;
            float: left;
            /* width: 100%; */
            text-align: left;
            height: 20px;
        }

        ul.stat_muat li span {
            float: left;
            text-align: left;
        }
        .dropdowns {
            float: left;
            margin-right: 70px;
            /* overflow: hidden; */
        }

        .dropdowns .dropbtn {
            font-size: 12px;
            border: none;
            outline: none;
            color: #1d1d1d;
            /* padding: 14px 16px; */
            /* background-color: #ff0000; */
            font-family: inherit;
            margin: 0;
            width: 90px;
        }

        .dropbtn:hover {
            cursor: pointer;
            background-color: grey;
        }

        .dropdowns:hover {
            background-color: #ff7b7b;
            text-decoration: none;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #ff0000 !important;
            min-width: 100px;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
            -webkit-transition: opacity .3s ease-in;
            -moz-transition: opacity .3s ease-in;
            -o-transition: opacity .3s ease-in;
            transition: opacity .3 ease-in;
        }

        .dropdown-content a {
            float: none;
            color: black;
            padding: 14px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #ff7b7b;
        }

        .show {
            display: block;
            z-index: 11;
        }
        .stat_muat .dropbtn {
            font-size: 12px;
            border: none;
            outline: none;
            color: #1d1d1d;
            font-family: inherit;
            margin: 0;
            width: 70px;
        }
        .stat_muat .dropbtn:hover {
            background-color: unset;
        }
        .btn_success {
            background-color: #17d017;
        }
        .btn_danger {
            background-color: red;
        }
    </style>
    <div class="container">
        <section class="chart_section">
            <div class="row">
                <div class="col-md-12 mb-4 align-items-stretch">
                    <div class="widthfull card card-shadow">
                        <div class="card-header">
                            <div class="card-title">
                               <span> Data Iklan</span>
                               <a href="{{ route('admin.berita.create') }}" class="btn btn-primary float-right">Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="bd-example table_style">

                                <table id="table" class="table table-sm ">
                                    <thead>
                                        <tr>
                                            <th>No NPWP / KTP</th>
                                            <th>No Invoice</th>
                                            <th>Nama Pemasang</th>
                                            <th>Nama Perusahaan</th>
                                            <th>Nama AE</th>
                                            <th>Judul Iklan</th>
                                            <th>Tanggal Tambah Berita</th>
                                            <th>Jenis Iklan</th>
                                            {{-- <th>Bulan Muat</th> --}}
                                            <th width="100">Tanggal Muat Iklan</th>
                                            {{-- <th>Status Dimuat</th> --}}
                                            <th>Sisa Pembayaran</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" language="javascript" src="{{ asset('public/admin/assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('public/admin/assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        // onclick tanggal muat
        $('body').on('click', '#dimuat', function() {
            let id = $(this).data('id');
            let tgl = $(this).data('tgl');

            Swal.fire({
                title: 'Anda yakin?',
                text: "akan mengganti status muat tanggal "+tgl,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya!'
            }).then((result) => {
                let url = "{{ route('admin.berita.dimuat', ['id' => 'ids']) }}";
                if (result.value) {
                    window.open(url.replace("ids", id),"_self")
                }
            })
        });

        $('body').on('click', '#belumdimuat', function() {
            let id = $(this).data('id');
            let tgl = $(this).data('tgl');

            Swal.fire({
                title: 'Anda yakin?',
                text: "akan mengganti status muat tanggal "+tgl,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya!'
            }).then((result) => {
                let url = "{{ route('admin.berita.bataldimuat', ['id' => 'ids']) }}";
                if (result.value) {
                    window.open(url.replace("ids", id),"_self")
                }
            })
        });

        $( document ).ready(function() {
            $('#table').DataTable().clear();
            $('#table').DataTable().destroy(false);
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.berita.dataTable') }}",
                columns: [
                    {data: 'no_id'},
                    {data: 'no_invoice'},
                    {data: 'nama_pemasang'},
                    {data: 'nama_perusahaan'},
                    {data: 'nama_ae'},
                    {data: 'judul_berita'},
                    {data: 'tanggal_berita'},
                    {data: 'nama_jenis_iklan'},
                    // {data: 'bulan_muat'},
                    {data: 'tanggal_muat_berita'},
                    // {data: 'status_dimuat'},
                    {data: 'sisa_bayar'},
                    {data: 'action'},
                ]
            });
        });    
            
        function myFunction(id) {
            console.log("tes");
            document.getElementById("myDropdown"+id).classList.toggle("show");
        }

        window.onclick = function(event) {
            if (!event.target.matches('.dropbtn')) {
                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }
    </script>

@endsection