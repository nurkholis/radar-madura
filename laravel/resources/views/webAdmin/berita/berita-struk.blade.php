@php
    use Config\Kholis as Helper;
@endphp
@extends('layouts.admin')
@section('css')
    <style>
        /* style sheet for "A4" printing */
        @media print and (width: 21cm) and (height: 29.7cm) {
            @page {
                margin: 3cm;
            }
        }

        /* style sheet for "letter" printing */
        @media print and (width: 8.5in) and (height: 11in) {
            @page {
                margin: 1in;
            }
        }

        /* A4 Landscape*/
        @page {
            size: 900px;
            /* margin: 10%; */
        }
        .struk{
            margin: 20px;
        }
        .struk .row{
            margin-bottom: 5px;
        }
        .struk .strip{
            margin: 0px 100px;
        }

        ul.list-tgl_muat {
            margin: 0;
            padding: 0;
            list-style: none;
        }
    </style>
@endsection
@section('content')

    <div class="container">
        <section class="chart_section">
            <div class="row">
                <div class="col-md-12 mb-4 align-items-stretch">
                    <div class="widthfull card card-shadow">

                        <div class="card-header">
                            <div class="card-title">
                                <span> Cetak Struk</span>
                                <button id="btn-print" class="btn btn-primary float-right">Cetak</button>
                                <a href="{{ route('admin.berita.index') }}" class="btn btn-danger float-right">Kembali</a>
                            </div>
                        </div>

                        <div class="card-body">
                                <div class="struk" id="print">
                                    <div class="kop text-center">
                                        <h1>RADAR MADURA</h1>
                                        <p>JL. Kabupaten, Pamekasan - Madura Telp.08343483483743</p>
                                        <hr>
                                    </div>
                                    <div class="isi">

                                            <div class="row">
                                                <div class="col-sm-4">Nama Pemasang</div>
                                                <div class="col-sm-6">:&nbsp;{{ $berita->nama_pemasang }}</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Nama Perusahaan</div>
                                                <div class="col-sm-6">:&nbsp;{{ $berita->nama_perusahaan }}</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Alamat</div>
                                                <div class="col-sm-6">:&nbsp;{{ $berita->alamat_pemasang }}</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Telepon</div>
                                                <div class="col-sm-6">:&nbsp;{{ $berita->telepon_pemasang }}</div>
                                            </div>                                    

                                            <div class="row">
                                                <div class="col-sm-4">No NPWP</div>
                                                <div class="col-sm-6">:&nbsp;{{ $berita->no_id }}</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">No Invoice</div>
                                                <div class="col-sm-6">:&nbsp;{{ $berita->no_invoice }}</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Jenis Iklan</div>
                                                <div class="col-sm-6">:&nbsp;{{ $berita->nama_jenis_iklan }}</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Ukuran Jumlah Pemuatan</div>
                                                <div class="col-sm-6">:&nbsp;{{ $berita->ukuran_kesamping }}mm Kesamping {{ $berita->ukuran_kebawah }}mm Kebawah</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Harga + PPN 10%</div>
                                                <div class="col-sm-6">:&nbsp;Rp.{{ $berita->harga }}&nbsp;,-</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Pembayaran</div>
                                                <div class="col-sm-6">:&nbsp;Rp.{{ $berita->dp_bayar }}&nbsp;,-</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Sisa Pembayaran</div>
                                                <div class="col-sm-6">:&nbsp;Rp.{{ $berita->sisa_bayar }}&nbsp;,-</div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">Tanggal Pemuatan</div>
                                                <div class="col-sm-6">:&nbsp; 
                                                    @if ($berita->tgl_muat != "")
                                                        <ul class="list-tgl_muat">
                                                        @foreach(explode(',', $berita->tgl_muat) as $tgl) 
                                                            <li>{{ $tgl }}</li>
                                                        @endforeach
                                                        </ul>
                                                    @endif
                                                </div>
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-6 text-center ttd">
                                                    <b>Mengetahui</b>
                                                    <br><br><br><br><br>
                                                    <hr class="strip">
                                                </div>
                                                <div class="col-sm-6 text-center ttd">
                                                    <b>Pemasang</b>
                                                    <br><br><br><br>
                                                    <p>{{ $berita->nama_pemasang }}</p>
                                                    <hr class="strip">
                                                </div>
                                            </div>

                                    </div>
                                </div>
                        </div>
                            
                    </div>
                </div>

            </div>
    </div>

@endsection

@section('js')
    <script>
        $("#btn-print").on("click", function() {
            printdiv('print')
        });
        function printdiv(printpage)
        {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            console.log(newstr);
            
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr+newstr+footstr;
            location.reload();
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
    </script>
@endsection