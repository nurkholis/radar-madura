@extends('layouts.admin')

@section('breadcrumb')
<a class="navbar-brand" href="{{ route('admin.home') }}">Beranda</a>/
@endsection

@section('content')
<div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12">
                 @if ($errors->any())
                      <div class="alert alert-danger alert-with-icon" data-notify="container">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <i class="fa fa-close"></i>
                          </button>
                          <span data-notify="message">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </span>
                      </div>
                  @endif
            </div>

            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Edit Profil</h4>
                  <p class="card-category">Biodata</p>
                </div>
                <div class="card-body">

                  <form method="POST" action="{{ route('admin.profil.put') }}" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="PUT">
                  {{ csrf_field() }}

                <div class="form-group">
                    <label class="bmd-label-floating text-capitalize">email</label>
                    <input type="text" class="form-control" name="email"
                    @if( isset($edit) ) value="{{ $edit->email }}" @else value="{{ old('email') }}" @endif required>
                </div>

                <div class="form-group">
                    <label class="bmd-label-floating text-capitalize">nama </label>
                    <input type="text" class="form-control" name="nama_user"
                    @if( isset($edit) ) value="{{ $edit->nama_user }}" @else value="{{ old('nama_user') }}" @endif required>
                </div>

                      <button type="submit" class="btn btn-success pull-right">Simpan</button>
                  </form>

                </div>
              </div>
            </div>
            
            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Edit Profil</h4>
                  <p class="card-category">Password</p>
                </div>
                <div class="card-body">

                  <form method="POST" action="{{ route('admin.profil.putPw') }}" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="PUT">
                  {{ csrf_field() }}

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password lama</label>
    <input type="text" class="form-control" name="password_lama"
     @if( isset($edit) ) value="{{ $edit->password_lama }}" @else value="{{ old('password_lama') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password baru</label>
    <input type="text" class="form-control" name="password_baru"
     @if( isset($edit) ) value="{{ $edit->password_baru }}" @else value="{{ old('password_baru') }}" @endif required>
</div>

<div class="form-group">
    <label class="bmd-label-floating text-capitalize">password konfirmasi</label>
    <input type="text" class="form-control" name="password_konfirmasi"
     @if( isset($edit) ) value="{{ $edit->password_konfirmasi }}" @else value="{{ old('password_konfirmasi') }}" @endif required>
</div>

                      <button type="submit" class="btn btn-success pull-right">Simpan</button>
                  </form>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
@endsection