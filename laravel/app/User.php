<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guard = 'user';
    protected $table = 'user';
    protected $primaryKey = 'id_user';
    public $timestamps = false;
    protected $guarded = [];
}
