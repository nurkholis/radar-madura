<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class JenisIklan extends Authenticatable
{
    use Notifiable;

    protected $guard = 'jenis_iklan';
    protected $table = 'jenis_iklan';
    protected $primaryKey = 'id_jenis_iklan';
    public $timestamps = false;
    protected $guarded = [];
}
