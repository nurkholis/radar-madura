<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class JenisProduk extends Authenticatable
{
    use Notifiable;

    protected $guard = 'jenis_produk';
    protected $table = 'jenis_produk';
    protected $primaryKey = 'id_jenis_produk';
    public $timestamps = false;
    protected $guarded = [];
}
