<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Berita extends Authenticatable
{
    use Notifiable;

    protected $guard = 'berita';
    protected $table = 'berita';
    protected $primaryKey = 'id_berita';
    public $timestamps = false;
    protected $guarded = [];
}
