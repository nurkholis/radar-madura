<?php

namespace App\Transformers;

use App\Berita;
use League\Fractal\TransformerAbstract;

class BeritaTransformer extends TransformerAbstract
{
    public function transform(Berita $data)
    {
        return [
            'id_berita' => $data->id_berita,
            'judul_berita' => $data->judul_berita,
            'cover_berita' => $data->cover_berita,
            'tanggal_berita' => $data->tanggal_berita,
            'admin' => [
                'nama_admin' => $data->nama_admin
            ]
        ];
    }
}