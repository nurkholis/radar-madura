<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\Berita;
use App\Transformers\BeritaTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class BeritaController extends Controller
{
    public function get(Request $request, Berita $Berita){
        $param = [];
        $limit = 15;
        $data = $Berita
            ->join('admin', 'admin.id_admin', '=', 'berita.id_admin')
            ->orderBy('berita.id_berita', 'desc');
            if($request->id_berita != "" ){
                $data = $data
                    ->where('berita.id_berita', $request->id_berita);
                $param['id_berita']=$request->id_berita;
            }
            if($request->search != "" ){
                $data = $data
                    ->where('berita.judul_berita', 'LIKE', '%'. $request->search . '%');
                $param['search']=$request->search;
            }
            if($request->limit != "" ){
                $limit = $request->limit;
                $param['limit']=$request->limit;
            }
            $data = $data->paginate($limit)
                ->appends($param);
        return fractal()
            ->collection($data)
            ->transformWith(new BeritaTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->addMeta([
            ])
            ->toArray();
    }
    
}
