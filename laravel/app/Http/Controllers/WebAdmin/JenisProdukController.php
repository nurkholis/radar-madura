<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\JenisProduk;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;

class JenisProdukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index(){
        return view('webAdmin.jenisProduk.jenisProduk-index');
    }
    public function dataTable(Request $request, JenisProduk $JenisProduk)
    {   
        $data = $JenisProduk
            ->orderBy('jenis_produk.id_jenis_produk', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.jenisProduk.edit', $data->id_jenis_produk) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.jenisProduk.delete', $data->id_jenis_produk) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.jenisProduk.jenisProduk-form');
    }
    public function store(Request $request, JenisProduk $JenisProduk )
    {
        $this->validate($request, [
            'nama_jenis_produk' => 'required'
        ]);

        $JenisProduk = $JenisProduk->create([
            'id_user' => Auth::guard('web')->user()->id_user,
            'nama_jenis_produk' => $request->nama_jenis_produk,
        ]);

        return redirect(route('admin.jenisProduk.index'));
    }
    public function edit($id, JenisProduk $JenisProduk)
    {
        $JenisProduk = $JenisProduk->find($id);
        return view('webAdmin.jenis_produk.jenis_produk-form', [
            'edit'  => $JenisProduk,
            ]);
    }
    public function update(Request $request, $id, JenisProduk $JenisProduk)
    {
        $JenisProduk = $JenisProduk->find($id);
        $JenisProduk->nama_jenisProduk = $request->get('nama_jenis_produk', $JenisProduk->nama_jenis_produk);
        $JenisProduk->save();

        return redirect(route('admin.jenisProduk.index'));
    }
    public function destroy($id, JenisProduk $JenisProduk)
    {
        $JenisProduk = $JenisProduk->find($id);
        $JenisProduk = $JenisProduk->delete();
        return back();
    }
    
}
