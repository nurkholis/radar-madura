<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\JenisIklan;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;

class JenisIklanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index(){
        return view('webAdmin.jenisIklan.jenisIklan-index');
    }
    public function dataTable(Request $request, JenisIklan $JenisIklan)
    {   
        $data = $JenisIklan
            ->orderBy('jenis_iklan.id_jenis_iklan', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.jenisIklan.edit', $data->id_jenis_iklan) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.jenisIklan.delete', $data->id_jenis_iklan) .'" class="btn btn-sm btn-danger round" onclick="return confirm(\'Data ini mungkin berkaitan dengan data Order iklan, seluruh data yang berkaitan dengan data ini juga akan ikut terhapus. Anda yakin?\')"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.jenisIklan.jenisIklan-form');
    }
    public function store(Request $request, JenisIklan $JenisIklan )
    {
        $this->validate($request, [
            'nama_jenis_iklan' => 'required'
        ]);

        $JenisIklan = $JenisIklan->create([
            'id_user' => Auth::guard('web')->user()->id_user,
            'nama_jenis_iklan' => $request->nama_jenis_iklan,
        ]);

        return redirect(route('admin.jenisIklan.index'));
    }
    public function edit($id, JenisIklan $JenisIklan)
    {
        $JenisIklan = $JenisIklan->find($id);
        return view('webAdmin.jenisIklan.jenisIklan-form', [
            'edit'  => $JenisIklan,
            ]);
    }
    public function update(Request $request, $id, JenisIklan $JenisIklan)
    {
        $JenisIklan = $JenisIklan->find($id);
        $JenisIklan->nama_jenis_iklan = $request->get('nama_jenis_iklan', $JenisIklan->nama_jenis_iklan);
        $JenisIklan->save();

        return redirect(route('admin.jenisIklan.index'));
    }
    public function destroy($id, JenisIklan $JenisIklan)
    {
        $JenisIklan = $JenisIklan->find($id);
        $JenisIklan = $JenisIklan->delete();
        return back();
    }
    
}
