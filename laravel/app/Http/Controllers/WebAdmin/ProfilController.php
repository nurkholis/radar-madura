<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\File;
use Auth;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index(Request $request, User $User)
    {   
        $param = [];
        $data = $User->find(Auth::guard('web')->user()->id_user);
        //dd($data);
        return view('webAdmin.profil.profil-index', ['edit' => $data]);
    }
    public function update(Request $request, User $User)
    {
        $this->validate($request, [
            'nama_user' => 'required|min:3',
        ]);
        $old = $User->find(Auth::guard('web')->user()->id_user);
        $User = $User->find(Auth::guard('web')->user()->id_user);

        if( $old->email != $request->email ){
            $this->validate($request, [
                'email' => 'required|unique:user'
            ]);
            $User->email = $request->email;
            $User->api_token = bcrypt($request->email);
        }
        $User->nama_user = $request->get('nama_user', $User->nama_user);
        $User->save();
        return redirect(route('admin.profil.index'));
    }
    public function updatePw(Request $request, User $User)
    {
        $this->validate($request, [
            'password_baru' => 'required|min:6',
        ]);
        
        if( $request->password_baru != $request->password_konfirmasi ){
            return back()->withErrors(['password tidak konfirmasi tidak sama']);
        }

        $credential = [
            'email' => Auth::guard('web')->user()->email,
            'password' => $request->password_lama,
        ];
        if(Auth::guard('web')->attempt($credential, $request->member)){

            $User = $User->find(Auth::guard('web')->user()->id_user);
            $User->password = bcrypt($request->password_baru);
            $User->api_token = bcrypt($request->email);
            $User->save();
            return redirect(route('admin.profil.index'));

        }else{
            return back()->withErrors(['password salah']);
        }
    }
    public function destroy($id, Sejarah $Sejarah)
    {
        $Sejarah = $Sejarah->find($id)->delete();
        return back();
    }
    
}
