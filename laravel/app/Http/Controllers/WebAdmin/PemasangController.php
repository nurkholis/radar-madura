<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pemasang;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;
use DB;

class PemasangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index(){
        return view('webAdmin.pemasang.pemasang-index');
    }
    public function dataTable(Request $request, Pemasang $Pemasang)
    {   
        $data = $Pemasang
            ->orderBy('pemasang.no_npwp', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.pemasang.edit', $data->no_id) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.pemasang.delete', $data->no_id) .'" class="btn btn-sm btn-danger round" onclick="return confirm(\'Data ini mungkin berkaitan dengan data Order iklan, seluruh data yang berkaitan dengan data ini juga akan ikut terhapus. Anda yakin?\')"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.pemasang.pemasang-form');
    }
    public function store(Request $request, Pemasang $Pemasang )
    {
        if ($request->pengenal == "ktp") {
            $this->validate($request, [
                'ktp' => 'required|unique:pemasang,no_id|min:16|numeric',
                'nama_pemasang' => 'required',
            ]);
        } else {
            $this->validate($request, [
                'npwp' => 'required|unique:pemasang,no_id',
                'nama_pemasang' => 'required',
            ]);
        }
        

        $Pemasang = $Pemasang->create([
            'pengenal' => $request->pengenal,
            'no_id' =>  $request->pengenal == "ktp" ?  $request->ktp :  $request->npwp,
            'nama_pemasang' => $request->nama_pemasang,
            'nama_perusahaan' => $request->nama_perusahaan,
            'telepon_pemasang' => $request->telepon_pemasang,
            'alamat_pemasang' => $request->alamat_pemasang,
        ]);

        return redirect(route('admin.pemasang.index'));
    }

    public function edit($id, Pemasang $Pemasang)
    {
        $Pemasang = $Pemasang->where('no_id', '=',  $id)->first();
        return view('webAdmin.pemasang.pemasang-form', [
            'edit'  => $Pemasang,
            ]);
    }

    public function update(Request $request, $id, Pemasang $Pemasang)
    { 
        $a = $Pemasang->where('no_id', '=',  $id)->first();
        $id = $request->pengenal == "ktp" ? $request->ktp: $request->npwp;
        if($a->no_id != $request->no_id){
            $a->no_id =  $id;
        }
        $a->nama_pemasang = $request->nama_pemasang;
        $a->nama_perusahaan = $request->nama_perusahaan;
        $a->telepon_pemasang = $request->telepon_pemasang;
        $a->alamat_pemasang = $request->alamat_pemasang;
        $a->save();
        return redirect(route('admin.pemasang.index'));
    }
    public function destroy($id, Pemasang $Pemasang)
    {
        $delete = Pemasang::where('id', $id)->delete();
        return back();
    }
    
}
