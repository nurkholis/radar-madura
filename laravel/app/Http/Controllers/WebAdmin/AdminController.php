<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index(){
        return view('webAdmin.admin.admin-index');
    }
    public function dataTable(Request $request, User $User)
    {   
        $data = $User
            ->orderBy('user.id_user', 'desc')
            ->where('level', '1')
            ->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.admin.edit', $data->id_user) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.admin.delete', $data->id_user) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.admin.admin-form');
    }
    public function store(Request $request, user $user )
    {
        $this->validate($request, [
            'nama_user' => 'required|min:3',
            'email' => 'required|email|unique:user',
            'password' => 'required|min:6',
        ]);

        $user = $user->create([
            'nama_user' => $request->nama_user,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'level' => '1',
        ]);

        return redirect(route('admin.admin.index'));
    }
    public function edit($id, User $User)
    {
        $User = $User->find($id);
        return view('webAdmin.admin.admin-form', [
            'edit'  => $User,
            ]);
    }
    public function update(Request $request, $id, User $User)
    {
        $User = $User->find($id);
        $User->nama_user = $request->get('nama_user', $User->nama_user);
        $User->email = $request->get('email', $User->email);
        $User->password = bcrypt($request->password);
        $User->save();

        return redirect(route('admin.admin.index'));
    }
    public function destroy($id, User $User)
    {
        $User = $User->find($id);
        $User = $User->delete();
        return back();
    }
    
}
