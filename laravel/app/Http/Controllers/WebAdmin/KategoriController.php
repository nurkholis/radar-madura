<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Kategori;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index(){
        return view('webAdmin.kategori.kategori-index');
    }
    public function dataTable(Request $request, Kategori $Kategori)
    {   
        $data = $Kategori
            ->orderBy('kategori.id_kategori', 'desc')->get();
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '
                <a href="'. route('admin.kategori.edit', $data->id_kategori) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit"></i> </a>
                <a href="'. route('admin.kategori.delete', $data->id_kategori) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash"></i> </a>
            ';
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    public function create()
    {
        return view('webAdmin.kategori.kategori-form');
    }
    public function store(Request $request, Kategori $Kategori )
    {
        $this->validate($request, [
            'nama_kategori' => 'required'
        ]);

        $Kategori = $Kategori->create([
            'id_user' => Auth::guard('web')->user()->id_user,
            'nama_kategori' => $request->nama_kategori,
        ]);

        return redirect(route('admin.kategori.index'));
    }
    public function edit($id, Kategori $Kategori)
    {
        $Kategori = $Kategori->find($id);
        return view('webAdmin.kategori.kategori-form', [
            'edit'  => $Kategori,
            ]);
    }
    public function update(Request $request, $id, Kategori $Kategori)
    {
        $Kategori = $Kategori->find($id);
        $Kategori->nama_kategori = $request->get('nama_kategori', $Kategori->nama_kategori);
        $Kategori->save();

        return redirect(route('admin.kategori.index'));
    }
    public function destroy($id, Kategori $Kategori)
    {
        $Kategori = $Kategori->find($id);
        $Kategori = $Kategori->delete();
        return back();
    }
    
}
