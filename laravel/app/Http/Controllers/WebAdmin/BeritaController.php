<?php

namespace App\Http\Controllers\WebAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Berita;
use App\JenisIklan;
use App\JenisProduk;
use App\Pemasang;
use Illuminate\Support\Facades\File;
use Auth;
use DataTables;
use Config\Kholis as Helper;
use Illuminate\Support\Facades\DB;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index(){
        return view('webAdmin.berita.berita-index');
    }
    public function dataTable(Request $request, Berita $Berita)
    {   
        // $data = $Berita
        //     ->join('pemasang', 'pemasang.no_npwp', '=', 'berita.no_npwp')
        //     ->join('jenis_iklan', 'jenis_iklan.id_jenis_iklan', '=', 'berita.id_jenis_iklan')
        //     ->join('user', 'user.id_user', '=', 'berita.id_user')
        //     ->orderBy('berita.id_berita', 'desc')->get();
        $data = DB::select(DB::raw("
            SELECT b.*, p.*, j.*, u.*,
            GROUP_CONCAT(b.status_dimuat) AS status_muat,
            GROUP_CONCAT(b.tanggal_muat_berita, ';', b.id_berita, ';', b.status_dimuat) AS tgl_muat
            FROM berita b 
            LEFT OUTER JOIN pemasang p ON b.no_npwp=p.no_id
            LEFT OUTER JOIN jenis_iklan j ON j.id_jenis_iklan=b.id_jenis_iklan
            LEFT OUTER JOIN USER u ON u.id_user=b.id_user
            GROUP BY b.id_user, b.judul_berita, b.tanggal_berita
            ORDER BY b.tanggal_berita
        "));
        // dd($data);
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            // if($data->dimuat){
                return '
                    <a href="'. asset('/public/files/berita').'/'.$data->file_berita .'" class="btn btn-sm btn-success round"> <i class="fa fa-download" title="download file berita"></i> </a>
                    <a href="'. route('admin.berita.edit', $data->ucode_berita) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit" title="edit data"></i> </a>
                    <a href="'. route('admin.berita.delete', $data->ucode_berita) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash" title="hapus data"></i> </a>
                ';
            // }else{
            //     return '
            //         <a href="'. route('admin.berita.dimuat', $data->id_berita) .'" class="btn btn-sm btn-warning round  onclick="confirmDimuat()"> <i class="fa fa-check" title="berita dimuat"></i> </a>
            //         <a href="'. asset('/public/files/berita').'/'.$data->file_berita .'" class="btn btn-sm btn-success round"> <i class="fa fa-download" title="download file berita"></i> </a>
            //         <a href="'. route('admin.berita.edit', $data->id_berita) .'" class="btn btn-sm btn-info round"> <i class="fa fa-edit" title="edit data"></i> </a>
            //         <a href="'. route('admin.berita.delete', $data->id_berita) .'" class="btn btn-sm btn-danger round" onclick="confirmDelete()"> <i class="fa fa-trash" title="hapus data"></i> </a>
            //     ';
            // }
           
        })
        ->addColumn('dimuat', function ($data) {
            if( $data->dimuat ){
                return '<span class="badge badge-pill badge-success float-right">Sudah</span>';
            }else{
                return '<span class="badge badge-pill badge-danger float-right">Belum</span>';
            }
        })
        ->addColumn('tanggal_berita', function ($data) {
            return Helper::tanggal($data->tanggal_berita);
        })
        ->addColumn('judul_berita', function ($data) {
            return '<a href="'. route('admin.berita.detail', $data->ucode_berita) .'">'.$data->judul_berita.'</>';
        })
        ->addColumn('tanggal_muat_berita', function ($data) {
            $tgl_muat = explode(",", $data->tgl_muat);
            $list_tgl = "<ul class='tgl_muat'>";

            foreach ($tgl_muat as $tgl) {
                $tmb = explode(";", $tgl);
                $status = $tmb[2];

                $bg = $status=="ya"?"btn_success":"btn_danger";
                $bg_confirm = $status=="ya"?"btn_danger":"btn_success";
                
                $list_tgl .= "<li>
                    <div class='dropdowns'>
                        <button class='dropbtn $bg' onclick='myFunction(".$tmb[1].")'>".$tmb[0]."</button>
                        <div class='dropdown-content' id='myDropdown".$tmb[1]."'>";
                if ($status == "ya") {
                    $list_tgl .= "<a class='$bg_confirm' href='#' style='color: white;' data-id='".$tmb[1]."' data-tgl='".$tmb[0]."' id='belumdimuat'>Belum</a>";
                } else {
                    $list_tgl .= "<a class='$bg_confirm' href='#' style='color: white;' data-id='".$tmb[1]."' data-tgl='".$tmb[0]."' id='dimuat'>Dimuat</a>";
                }
                
                $list_tgl .= "</div>
                    </div>
                </li>";
                
            }
            $list_tgl .= "</ul>";

            return $list_tgl;
        })
        // ->addColumn('status_dimuat', function ($data) {
        //     $stat_muat = "";
        //     $stats = explode(",", $data->status_muat);

        //     $stat_muat = "<ul class='stat_muat'>";
        //     foreach ($stats as $s) {  
        //         if ($s == "ya") {
        //             $stat_muat .= "<li><button class='dropbtn btn_success'>Dimuat</button></li>";
        //         } else {
        //             $stat_muat .= "<li><button class='dropbtn btn_danger'>Belum</button></li>";
        //         }
        //     }
        //     $stat_muat .= "</ul>";

        //     return $stat_muat;
        // })
        // ->addColumn('tanggal_muat', function ($data) {
        //     $a = unserialize($data->tanggal_muat);
        //     $hasil = '';
        //     for ($i=0; $i < count($a); $i++) { 
        //         $hasil .= '<span class="badge badge-pill badge-info">' . $a[$i] . '</span> ';
        //     }
        //     return $hasil;
        // })
        ->rawColumns(['dimuat', 'tanggal_berita', 'judul_berita', 'tanggal_muat', 'action','tanggal_muat_berita','status_dimuat'])
        ->make(true);
    }

    public function detail($id, Berita $Berita)
    {
        // $Berita = $Berita
        //     ->join('pemasang', 'pemasang.no_npwp', '=', 'berita.no_npwp')
        //     ->join('jenis_iklan', 'jenis_iklan.id_jenis_iklan', '=', 'berita.id_jenis_iklan')
        //     ->find($id);
        $Berita = DB::select(DB::raw("
            SELECT b.*, p.*, j.*, u.*,
            GROUP_CONCAT(b.status_dimuat) AS status_muat,
            GROUP_CONCAT(b.tanggal_muat_berita, ';', b.status_dimuat) AS tgl_muat
            FROM berita b 
            LEFT OUTER JOIN pemasang p ON b.no_npwp=p.no_id
            LEFT OUTER JOIN jenis_iklan j ON j.id_jenis_iklan=b.id_jenis_iklan
            LEFT OUTER JOIN USER u ON u.id_user=b.id_user
            WHERE ucode_berita='$id'
            GROUP BY b.id_user, b.judul_berita, b.tanggal_berita
            ORDER BY b.tanggal_berita
        "));

        return view('webAdmin.berita.berita-detail', [
            'berita' => $Berita[0],
        ]);
    }

    public function dimuat($id, Berita $Berita)
    {
        $Berita = $Berita->find($id);
        $Berita->status_dimuat = 'ya';
        $Berita->save();
        return redirect(route('admin.berita.index'));
    }

    public function bataldimuat($id, Berita $Berita)
    {
        $Berita = $Berita->find($id);
        $Berita->status_dimuat = 'tidak';
        $Berita->save();
        return redirect(route('admin.berita.index'));
    }

    public function create(JenisIklan $JenisIklan, Pemasang $Pemasang)
    {
        $Pemasang = $Pemasang->all();
        $JenisIklan = $JenisIklan->all();
        return view('webAdmin.berita.berita-form', [
            'jenis_iklan' => $JenisIklan,
            'pemasang' => $Pemasang,
        ]);
    }

    public function store(Request $request, Berita $Berita )
    {
        $this->validate($request, [
            'cover_berita' => $request->cover_berita ? 'max:2000' : '',
            'file_berita' => $request->file_berita ? 'max:2000' : '',
        ]);

        $cover_berita = "";
        if ($request->file('cover_berita')) {
            $destinationPath = "public/images/berita";
            $image = $request->file('cover_berita');
            $cover_berita = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $cover_berita);
        }
        
        $file_berita = "";
        if ($request->file('file_berita')) {
            $destinationPath = "public/files/berita";
            $image = $request->file('file_berita');
            $file_berita = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $file_berita);
        }        

        // status dimuat
        $tgl_dimuat = explode(",", $request->tanggal_muat_berita);
        $status_dimuat  = array();
        $ucode = rand(1111111111,9999999999);

        foreach ($tgl_dimuat as $tgl) {
            // $status_dimuat[] = "$tgl;tidak";
            $Berita = $Berita->create([
                'id_user' => Auth::guard('web')->user()->id_user,
                'id_jenis_iklan' => $request->id_jenis_iklan,
                'no_npwp' => $request->no_npwp,
                'judul_berita' => $request->judul_berita,
                'file_berita' => $file_berita,
                'tanggal_berita' => $request->tanggal_berita,
                'cover_berita' => $cover_berita,
                'ukuran_kesamping' => $request->ukuran_kesamping,
                'ukuran_kebawah' => $request->ukuran_kebawah,
                'harga' => $request->harga,
                'tanggal_muat' => $this->cacahTanggalMuat($request->tanggal_muat),
                'bulan_muat' => $request->bulan_muat,
                'dimuat' => '0',
                'no_invoice' => $request->no_invoice,
                'dp_bayar' => $request->dp_bayar,
                'sisa_bayar' => $request->sisa_bayar,
                'nama_ae' => $request->nama_ae,
                'tanggal_muat_berita' => $tgl,
                'status_dimuat' => "tidak",
                'ucode_berita' => $ucode
            ]);
        }

        return redirect(route('admin.berita.struk', ['ucode' => $ucode]));
    }

    public function cacahTanggalMuat($array)
    {
        $serial = serialize( $array );
        $unserial = unserialize( $serial);
        return $serial;
    }

    public function struk($id, Berita $Berita){
        // $Berita = $Berita
        //     ->join('pemasang', 'pemasang.no_npwp', '=', 'berita.no_npwp')
        //     ->join('jenis_iklan', 'jenis_iklan.id_jenis_iklan', '=', 'berita.id_jenis_iklan')
        //     ->orderBy('berita.id_berita', 'desc')->take(1)->get();
        $Berita = DB::select(DB::raw("
            SELECT b.*, p.*, j.*, u.*,
            GROUP_CONCAT(b.status_dimuat) AS status_muat,
            GROUP_CONCAT(b.tanggal_muat_berita) AS tgl_muat
            FROM berita b 
            LEFT OUTER JOIN pemasang p ON b.no_npwp=p.no_id
            LEFT OUTER JOIN jenis_iklan j ON j.id_jenis_iklan=b.id_jenis_iklan
            LEFT OUTER JOIN USER u ON u.id_user=b.id_user
            WHERE ucode_berita='$id'
            GROUP BY b.id_user, b.judul_berita, b.tanggal_berita
            ORDER BY b.tanggal_berita
        "));
        //dd($Berita);
        return view('webAdmin.berita.berita-struk', [
            'berita' => $Berita[0],
        ]);
    }
    public function edit($id, Berita $Berita, JenisIklan $JenisIklan, Pemasang $Pemasang)
    {
        $JenisIklan = $JenisIklan->all();
        $Pemasang = $Pemasang->all();
        // $Berita = $Berita->find($id);

        $Berita = DB::select(DB::raw("
            SELECT b.*,
            GROUP_CONCAT(b.status_dimuat) AS status_muat,
            GROUP_CONCAT(b.tanggal_muat_berita) AS tgl_muat, p.no_id
            FROM berita b 
            Left outer join pemasang p on p.no_id=b.no_npwp
            WHERE ucode_berita='$id'
            GROUP BY b.id_user, b.judul_berita, b.tanggal_berita
        "));
        // dd($Berita);
        return view('webAdmin.berita.berita-form', [
            'edit'  => $Berita[0],
            'jenis_iklan' => $JenisIklan,
            'pemasang' => $Pemasang,
        ]);
    }

    public function update(Request $request, $id, Berita $Berita)
    {
        $berita = new Berita();

        $data = DB::select(DB::raw("
            SELECT b.*
            FROM berita b 
            WHERE ucode_berita='$id'
        "));
        
        $tgl_muat = array();
        foreach ($data as $key => $value) {
            // dd($value->tanggal_muat_berita);
            $tgl_muat[] = $value->tanggal_muat_berita;
        }

        $cover_berita = "";
        if ($request->file('cover_berita')) {
            $destinationPath = "public/images/berita";
            $image = $request->file('cover_berita');
            $cover_berita = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $cover_berita);
        }
        
        $file_berita = "";
        if ($request->file('file_berita')) {
            $destinationPath = "public/files/berita";
            $image = $request->file('file_berita');
            $file_berita = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $file_berita);
        }

        $tgl_dimuat = explode(",", $request->tanggal_muat_berita);
        foreach ($tgl_dimuat as $tgl) {
            if (in_array($tgl, $tgl_muat)) {
                # update jika tanggal sama
                $update = Berita::where('tanggal_muat_berita', $tgl)
                ->where('ucode_berita', $id)
                ->update([
                    'cover_berita' => $cover_berita,
                    'file_berita' => $file_berita,
                    'judul_berita' => $request->get('judul_berita'),
                    'id_jenis_iklan' => $request->get('id_jenis_iklan'),
                    'no_npwp' => $request->get('no_npwp'),
                    'tanggal_berita' => $request->get('tanggal_berita'),
                    'ukuran_kesamping' => $request->get('ukuran_kesamping'),
                    'ukuran_kebawah' => $request->get('ukuran_kebawah'),
                    'harga' => $request->get('harga'),
                    'bulan_muat' => $request->get('bulan_muat'),
                    'tanggal_muat' => $this->cacahTanggalMuat($request->tanggal_muat),
                    'no_invoice' => $request->get('no_invoice'),
                    'sisa_bayar' => $request->get('sisa_bayar'),
                    'dp_bayar' => $request->get('dp_bayar'),
                    'nama_ae' => $request->get('nama_ae'),
                    'tanggal_muat_berita' => $tgl,
                ]);
            } else {
                # insert new record jika ada tanggal baru
                $Berita = $Berita->create([
                    'id_user' => Auth::guard('web')->user()->id_user,
                    'cover_berita' => $cover_berita,
                    'file_berita' => $file_berita,
                    'judul_berita' => $request->get('judul_berita'),
                    'id_jenis_iklan' => $request->get('id_jenis_iklan'),
                    'no_npwp' => $request->get('no_npwp'),
                    'tanggal_berita' => $request->get('tanggal_berita'),
                    'ukuran_kesamping' => $request->get('ukuran_kesamping'),
                    'ukuran_kebawah' => $request->get('ukuran_kebawah'),
                    'harga' => $request->get('harga'),
                    'bulan_muat' => $request->get('bulan_muat'),
                    'tanggal_muat' => $this->cacahTanggalMuat($request->tanggal_muat),
                    'no_invoice' => $request->get('no_invoice'),
                    'sisa_bayar' => $request->get('sisa_bayar'),
                    'dp_bayar' => $request->get('dp_bayar'),
                    'nama_ae' => $request->get('nama_ae'),
                    'tanggal_muat_berita' => $tgl,
                    'status_dimuat' => "tidak",
                    'ucode_berita' => $id
                ]);
            }
        }

        // hapus
        foreach ($data as $key => $value) {
            if (!in_array($value->tanggal_muat_berita, $tgl_dimuat)) {
                $delete = Berita::where('id_berita', $value->id_berita)->delete();
            }
        }
        return redirect(route('admin.berita.index'));
    }
    
    /*public function update(Request $request, $id, Berita $Berita)
    {
        $Berita = $Berita->find($id);

        if(!$request->cover_berita  == null ){
            $image_path = $destinationPath = "public/images/berita/".$Berita->cover_berita;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/berita";
            $image = $request->file('cover_berita');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Berita->cover_berita = $image_name;
        }

        if(!$request->file_berita  == null ){
            $image_path = $destinationPath = "public/files/berita/".$Berita->file_berita;     
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $destinationPath = "public/images/files";
            $image = $request->file('file_berita');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $image_name);
            $Berita->file_berita = $image_name;
        }

        $Berita->judul_berita = $request->get('judul_berita', $Berita->judul_berita);
        $Berita->id_jenis_iklan = $request->get('id_jenis_iklan', $Berita->id_jenis_iklan);
        $Berita->no_npwp = $request->get('no_npwp', $Berita->no_npwp);
        $Berita->tanggal_berita = $request->get('tanggal_berita', $Berita->tanggal_berita);
        $Berita->ukuran_kesamping = $request->get('ukuran_kesamping', $Berita->ukuran_kesamping);
        $Berita->ukuran_kebawah = $request->get('ukuran_kebawah', $Berita->ukuran_kebawah);
        $Berita->harga = $request->get('harga', $Berita->harga);
        $Berita->bulan_muat = $request->get('bulan_muat', $Berita->bulan_muat);
        $Berita->tanggal_muat = $this->cacahTanggalMuat($request->tanggal_muat);
        $Berita->no_invoice = $request->get('no_invoice', $Berita->no_invoice);
        $Berita->sisa_bayar = $request->get('sisa_bayar', $Berita->sisa_bayar);
        $Berita->dp_bayar = $request->get('dp_bayar', $Berita->dp_bayar);
        $Berita->nama_ae = $request->get('nama_ae', $Berita->nama_ae);
        $Berita->save();

        return redirect(route('admin.berita.index'));
    } */

    public function destroy($id, Berita $Berita)
    {
        // $Berita = $Berita->find($id);
        $delete = Berita::where('ucode_berita', $id)->delete();

        // $image_path = "public/images/berita/".$Berita->cover_berita;       
        // if (File::exists($image_path)) {
        //     File::delete($image_path);
        // }
        // $image_path = "public/files/berita/".$Berita->file_berita;       
        // if (File::exists($image_path)) {
        //     File::delete($image_path);
        // }
        // $Berita = $Berita->delete();
        return back();
    }

    public function data($key, Pemasang $Pemasang){
        $data = $Pemasang
            ->orderBy('no_npwp', 'desc');
        if($key != 0){
            $data = $data->where('no_npwp', 'LIKE', '%' . $key . '%' )
                        ->orWhere('nama_pemasang', 'LIKE', '%' . $key . '%' );
        }
        $data = $data->get();

        if(count($data)>0){
            $response = response()->json([
                        'status' => 200,
                        'data' =>$data]);
        }else{
            $response = response()->json([
                'status' => 400,
                'data' =>$data]);
        }
        return $response;
    }
    
}
