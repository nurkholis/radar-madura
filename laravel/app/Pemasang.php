<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pemasang extends Authenticatable
{
    use Notifiable;

    protected $guard = 'pemasang';
    protected $table = 'pemasang';
    protected $primaryKey = 'no_id';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    protected $guarded = [];
}
