<?php

Route::prefix('/')->group( function() {

    Route::get('', 'WebAdmin\HomeController@index')->name('admin.home');
    Route::get('login', 'AuthAdmin\LoginController@showloginForm')->name('admin.login');
    Route::post('login', 'AuthAdmin\LoginController@login')->name('admin.login.submit');
    Route::get('logout', function(){
        Auth::logout();
        Session::flush();
        return redirect(route('admin.login'));
    })->name('admin.logout'); 

    Route::prefix('profil')->group( function() {
        Route::get('/', 'WebAdmin\ProfilController@index')->name('admin.profil.index');
        Route::put('/', 'WebAdmin\ProfilController@update')->name('admin.profil.put');
        Route::put('/pw', 'WebAdmin\ProfilController@updatePw')->name('admin.profil.putPw');
    });

    Route::prefix('kategori')->group( function() {
        Route::get('/', 'WebAdmin\KategoriController@index')->name('admin.kategori.index');
        Route::get('/dataTable', 'WebAdmin\KategoriController@dataTable')->name('admin.kategori.dataTable');
        Route::get('/form', 'WebAdmin\KategoriController@create')->name('admin.kategori.create');
        Route::get('/form/{id}', 'WebAdmin\KategoriController@edit')->name('admin.kategori.edit');
        Route::post('/', 'WebAdmin\KategoriController@store')->name('admin.kategori.store');
        Route::get('/delete/{id}', 'WebAdmin\KategoriController@destroy')->name('admin.kategori.delete');
        Route::put('/{id}', 'WebAdmin\KategoriController@update')->name('admin.kategori.put');
    });

    Route::prefix('admin')->group( function() {
        Route::get('/', 'WebAdmin\AdminController@index')->name('admin.admin.index');
        Route::get('/dataTable', 'WebAdmin\AdminController@dataTable')->name('admin.admin.dataTable');
        Route::get('/form', 'WebAdmin\AdminController@create')->name('admin.admin.create');
        Route::get('/form/{id}', 'WebAdmin\AdminController@edit')->name('admin.admin.edit');
        Route::post('/', 'WebAdmin\AdminController@store')->name('admin.admin.store');
        Route::get('/delete/{id}', 'WebAdmin\AdminController@destroy')->name('admin.admin.delete');
        Route::put('/{id}', 'WebAdmin\AdminController@update')->name('admin.admin.put');
    });

    Route::prefix('pemasang')->group( function() {
        Route::get('/', 'WebAdmin\PemasangController@index')->name('admin.pemasang.index');
        Route::get('/dataTable', 'WebAdmin\PemasangController@dataTable')->name('admin.pemasang.dataTable');
        Route::get('/form', 'WebAdmin\PemasangController@create')->name('admin.pemasang.create');
        Route::get('/form/{id}', 'WebAdmin\PemasangController@edit')->name('admin.pemasang.edit');
        Route::post('/', 'WebAdmin\PemasangController@store')->name('admin.pemasang.store');
        Route::get('/delete/{id}', 'WebAdmin\PemasangController@destroy')->name('admin.pemasang.delete');
        Route::put('/{id}', 'WebAdmin\PemasangController@update')->name('admin.pemasang.put');
    });

    Route::prefix('jenis/iklan')->group( function() {
        Route::get('/', 'WebAdmin\JenisIklanController@index')->name('admin.jenisIklan.index');
        Route::get('/dataTable', 'WebAdmin\JenisIklanController@dataTable')->name('admin.jenisIklan.dataTable');
        Route::get('/form', 'WebAdmin\JenisIklanController@create')->name('admin.jenisIklan.create');
        Route::get('/form/{id}', 'WebAdmin\JenisIklanController@edit')->name('admin.jenisIklan.edit');
        Route::post('/', 'WebAdmin\JenisIklanController@store')->name('admin.jenisIklan.store');
        Route::get('/delete/{id}', 'WebAdmin\JenisIklanController@destroy')->name('admin.jenisIklan.delete');
        Route::put('/{id}', 'WebAdmin\JenisIklanController@update')->name('admin.jenisIklan.put');
    });

    Route::prefix('jenis/produk')->group( function() {
        Route::get('/', 'WebAdmin\JenisProdukController@index')->name('admin.jenisProduk.index');
        Route::get('/dataTable', 'WebAdmin\JenisProdukController@dataTable')->name('admin.jenisProduk.dataTable');
        Route::get('/form', 'WebAdmin\JenisProdukController@create')->name('admin.jenisProduk.create');
        Route::get('/form/{id}', 'WebAdmin\JenisProdukController@edit')->name('admin.jenisProduk.edit');
        Route::post('/', 'WebAdmin\JenisProdukController@store')->name('admin.jenisProduk.store');
        Route::get('/delete/{id}', 'WebAdmin\JenisProdukController@destroy')->name('admin.jenisProduk.delete');
        Route::put('/{id}', 'WebAdmin\JenisProdukController@update')->name('admin.jenisProduk.put');
    });

    Route::prefix('berita')->group( function() {
        Route::get('/', 'WebAdmin\BeritaController@index')->name('admin.berita.index');
        Route::get('/data/{key}', 'WebAdmin\BeritaController@data')->name('admin.berita.data');
        Route::get('/dataTabel', 'WebAdmin\BeritaController@dataTable')->name('admin.berita.dataTable');
        Route::get('/struk/{ucode}', 'WebAdmin\BeritaController@struk')->name('admin.berita.struk');
        Route::get('/dimuat/{id}', 'WebAdmin\BeritaController@dimuat')->name('admin.berita.dimuat');
        Route::get('/bataldimuat/{id}', 'WebAdmin\BeritaController@bataldimuat')->name('admin.berita.bataldimuat');
        Route::get('/form', 'WebAdmin\BeritaController@create')->name('admin.berita.create');
        Route::get('/form/{id}', 'WebAdmin\BeritaController@edit')->name('admin.berita.edit');
        Route::get('/detail/{id}', 'WebAdmin\BeritaController@detail')->name('admin.berita.detail');
        Route::post('/', 'WebAdmin\BeritaController@store')->name('admin.berita.store');
        Route::get('/delete/{id}', 'WebAdmin\BeritaController@destroy')->name('admin.berita.delete');
        Route::put('/{id}', 'WebAdmin\BeritaController@update')->name('admin.berita.put');
    });

});


